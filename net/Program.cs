﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace dnet
{
  class Program
  {


    static List<string> good_emails = new List<string>() {
        "john.doe@gmail.com",
        "hello.world@aaa-calif.com",
        "hello.world@aaa-calif.it",
        "ay._+-dsa@gmail.com",
        "_aaa.testing@clublabs.com"
    };

    static List<string> bad_emails = new List<string>()
    {
        "john$doe@gmail.com",
        "john.doe@gmail",
        "caplus",
        "ay._+-@gmail.com",
        "aaaaaa-@gmail.com",
        "aaaaaa+@gmail.com",
        "aaaaaa_@gmail.com",
        "aaaaaa.@gmail.com",
        "abc.def@clublabs123.com2",

        "+aaa.testing@clublabs.com",
        ".aaa.testing@clublabs.com",
        "-aaa.testing@clublabs.com",
        "hello.world%@aaa-calif.it",
        "%hello.world%@aaa-calif.it"
    };

    static bool assert_email(string email) {
        bool myresult = false;
        try
        {
            MailAddress emailAddress = new MailAddress(email);
            myresult = true; 
        }
        catch (System.Exception)
        {
        }
        return myresult;
    }

    static void Main(string[] args)
    {
        Console.WriteLine("*** emails list     ***\n");
        foreach(var e in good_emails) {
            var result = assert_email(e);
            Console.WriteLine($"{e} : {(result?"":"NOT AN EMAIL")}");
        }
        Console.WriteLine("\n*** non-emails list ***\n");
        foreach(var e in bad_emails) {
            var result = assert_email(e);
            Console.WriteLine($"{e} : {(result?"":"NOT AN EMAIL")}");
        }
    }
  }
}
