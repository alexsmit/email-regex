var reg_array = [
  // /^([^<>()\[\]\\.,;:\s@""]+(\.[^<>()\[\]\\.,;:\s@""]+)*)@(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})$/,
  
  // /^[A-Za-z0-9](([_\.\-+]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/,
  /^[A-Za-z0-9_](([_\.\-+]*[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/,
  
  // /^[a-zA-Z0-9]+[-.+\w]*[a-zA-Z0-9]+@\w+([-_.+]*\w*)+?\.\w+([-_.+]*\w*){2,3}$/
];

var reg_list  = [
  /^.*@/,
  /^[A-Za-z0-9_](([_\.\-+]*[a-zA-Z0-9]+)*)@/, // before @ symbol entered: alex@
  /^@([\-.A-Za-z0-9]+)$/, // allows any alphanum-.  after @ symbol entered: alex@aaa- alex@calif-aaa.
     /^.*@.*\.([A-Za-z]{2,})$/, // last part is a domain => check full regex
  // alex@ddd.dd  @yahoo.com.edu
];

var reg_title = [
  // "SSO", 
  "Registration", 
  // "notification"
]

function assert_email(reg, email, result) {
  const isemail = reg.test(String(email));
  console.log(email, ":", (isemail===result)?"OK":"FAIL");
}

var good_emails = [
  "john.doe@gmail.com",
  "hello.world@aaa-calif.com",
  "hello.world@aaa-calif.it",
  "ay._+-dsa@gmail.com",
  "_aaa.testing@clublabs.com",
  // new items
  "_aaa.testing@clublabs.com.edu",
  "aaa.testing@clublabs.com.edu",
  "hello.world@aaa-calif.aaa-calif.com",
];

var bad_emails = [
  "john$doe@gmail.com-",
  "john$doe@gmail..com",
  "john$doe@gmail.com.e",
  "john.doe@gmail",
  "caplus",
  "ay._+-@gmail.com",
  "aaaaaa-@gmail.com",
  "aaaaaa+@gmail.com",
  "aaaaaa_@gmail.com",
  "aaaaaa.@gmail.com",
  "abc.def@clublabs123.com2",

  "+aaa.testing@clublabs.com",
  ".aaa.testing@clublabs.com",
  "-aaa.testing@clublabs.com",
  "hello.world%@aaa-calif.it",
  "%hello.world%@aaa-calif.it",
];


for(var i=0; i< reg_array.length; i++) {
  console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  console.log('>>> ', reg_title[i]);
  console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
  console.log("emails\n")
  good_emails.forEach(element => {
    assert_email(reg_array[i],element,true)
  });
  
  console.log("\nnon-emails\n")
  bad_emails.forEach(element => {
    assert_email(reg_array[i],element,false)
  });

}




// assert_email("a.b12@gmail.com",true)
// assert_email("a+b12@gmail.com",true)
// assert_email("a_b12@gmail.com",true)

// assert_email("a_b12@gmail.cc",true)
// assert_email("a_b12@aaa-calif.com",true)
// assert_email("a_b12@super.aaa-calif.com",true)

// console.log("\nnon-emails\n")

// assert_email("a_b12@_gmail.com",false)

// assert_email("_a_b12@gmail",false)
// assert_email("_a_b12@gmail.c",false)
// assert_email("_a_b12",false)
